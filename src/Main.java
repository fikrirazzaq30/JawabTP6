
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Juvetic
 */
public class Main {
    
    public static void main(String[] args) {
        Pelanggan p1 = new Pelanggan(1, "Dybala", "Italia");
        Pelanggan p2 = new Pelanggan(2, "Mandzukic", "Italia");
        Main m = new Main();
        m.savePelanggan(p1);
        m.getPelanggan(1);
        m.deletePelanggan(2);
    }
    
    public void savePelanggan(Pelanggan p) {
        try {
            DBConnection dbcon = new DBConnection();
            String sql = "INSERT INTO pelanggan VALUES('" + p.getId() + "','" + p.getNama() + "','" + p.getAddress() + "')";
            dbcon.stmt = dbcon.con.createStatement();
            dbcon.stmt.executeUpdate(sql);
            System.out.println("Data berhasil disimpan!");
        } catch (SQLException ex) {
            System.out.println("Error " + ex);
        }
    }
    
    public Pelanggan getPelanggan(int id) {
        Pelanggan p = null;
        try {
            DBConnection dbcon = new DBConnection();
            String sql = "SELECT * FROM pelanggan WHERE id=" + id;
            dbcon.stmt = dbcon.con.createStatement();
            dbcon.rs = dbcon.stmt.executeQuery(sql);
            
            //Looping ambil record di database
            while(dbcon.rs.next()){
                int idp = dbcon.rs.getInt("id");
                String nama = dbcon.rs.getString("nama");
                String address = dbcon.rs.getString("address");
                p = new Pelanggan(idp, nama, address);
                
                //Output isi
                System.out.format("%s, %s, %s\n", idp, nama, address);
            }
            //Return
        } catch (SQLException ex) {
            System.out.println("Error " + ex);        
        }
        return p;
    }
    
    public void deletePelanggan(int id) {
        try {
            DBConnection dbcon = new DBConnection();
            String sql = "DELETE FROM pelanggan WHERE id=" + id;
            dbcon.stmt = dbcon.con.createStatement();
            dbcon.stmt.executeUpdate(sql);
            System.out.println("Data berhasil dihapus!");
        } catch (SQLException ex) {
            System.out.println("Error " + ex);
        }
    }}

